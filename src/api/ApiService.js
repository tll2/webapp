const BASE_URL = 'http://localhost:5000/api/front/';

export const fetchData = async () => {
  try {
    const response = await fetch(`${BASE_URL}data`, {
      method: 'GET',
    });
    if (!response.ok) throw new Error('Network response was not ok.');
    return await response.json();
  } catch (error) {
    console.error("Fetch error: ", error);
    throw error;
  }
};

export const submitData = async (data) => {
  try {
    const response = await fetch(`${BASE_URL}submit`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
    if (!response.ok) throw new Error('Network response was not ok.');
    return await response.json();
  } catch (error) {
    console.error("Submit error: ", error);
    throw error;
  }
};

import React from 'react';
import useTelegramWebApp from '../hooks/useTelegramWebApp';

function TelegramWebAppButton() {
  const { close } = useTelegramWebApp();

  return (
    <button onClick={close}>Закрыть WebApp</button>
  );
}

export default TelegramWebAppButton;

import React, { useState } from 'react';
import { submitData } from '../api/ApiService';
import TelegramWebAppButton from './TelegramWebAppButton';
import './FormPage.css';

const FormPage = () => {
  const [data, setData] = useState('');

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await submitData({ data });
      console.log(response);
      // Redirect or display a message based on response
    } catch (error) {
      console.error(error);
    }
  };

  return (

  <div className="container">
  <form onSubmit={handleSubmit} className="form">
    <label htmlFor="dataInput" className="label">Data:</label>
    <input
      id="dataInput"
      name="data"
      type="text"
      className="input"
      value={data}
      onChange={(e) => setData(e.target.value)}
      required
    />
    <button type="submit" className="button">Submit</button>
  </form>
  <div className="content">
    {/* Содержимое компонента */}
    <TelegramWebAppButton/>
  </div>
</div>

  );
};

export default FormPage;

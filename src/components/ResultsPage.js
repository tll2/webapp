import React, { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom'; // Это было пропущено в импорте
import { fetchData } from '../api/ApiService';
import TelegramWebAppButton from './TelegramWebAppButton';
import './ResultsPage.css';

const ResultsPage = () => {
  const { search } = useLocation();
  const queryParams = new URLSearchParams(search);

  const [totalReg, setTotalReg] = useState(queryParams.get('total_reg'));
  const [name, setName] = useState(queryParams.get('name'));
  const [surname, setSurname] = useState(queryParams.get('surname'));
  const [results, setResults] = useState(null); // Добавлено состояние для результатов

  useEffect(() => {
    const getData = async () => {
      try {
        const data = await fetchData(); // Убедись, что функция fetchData() принимает нужные параметры, если это необходимо
        setResults(data);
      } catch (error) {
        console.error(error);
      }
    };

    getData();
  }, [totalReg, name, surname]);

  return (
    <>
      <div class="results-container">
  <h1 class="results-title">Results</h1>
  <p>Total Registered: {totalReg}</p>
  <p>Name: {name}</p>
  <p>Surname: {surname}</p>
  <button class="results-button">Закрыть WebApp</button>
</div>
    </>
  );
};

export default ResultsPage;

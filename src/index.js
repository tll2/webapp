import React from 'react';
import { StrictMode } from 'react';
import { createRoot } from 'react-dom/client'; // Обновленный импорт
import App from './App';

// Получаем элемент, куда будет производиться рендеринг
const container = document.getElementById('root');

// Создаем корень и рендерим приложение
const root = createRoot(container); // Используем createRoot
root.render(<StrictMode>
    <App />
  </StrictMode>
  );
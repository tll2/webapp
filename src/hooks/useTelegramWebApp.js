import { useEffect } from 'react';

function useTelegramWebApp() {
  useEffect(() => {
    if (window.Telegram?.WebApp) {
      window.Telegram.WebApp.ready();
      // Можно добавить дополнительные настройки здесь
    }
  }, []);

  const close = () => {
    if (window.Telegram?.WebApp) {
      window.Telegram.WebApp.close();
    }
  };

  return { close };
}

export default useTelegramWebApp;
# Базовый образ Node.js
FROM node:latest

# Установка рабочей директории в контейнере
WORKDIR /app

# Копирование файлов `package.json` и `package-lock.json` (или `yarn.lock`)
COPY package*.json ./

# Установка зависимостей проекта
RUN npm install

# Копирование исходного кода в контейнер
COPY . .

# Сборка приложения для продакшена
RUN npm run build

# Установка сервера для обслуживания статики
RUN npm install -g serve

# Открытие порта, который будет прослушиваться на runtime
EXPOSE 5000

# Запуск приложения
CMD ["serve", "-s", "build", "-l", "5000"]
